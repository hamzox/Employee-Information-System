/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeemanagementsystem;

/**
 *
 * @author Shayan
 */
class Node {

    Node prev;
    Node next;
    int data;

    String nam;
    String des;
    String sal;
    String dept;
    String age;
    String add;
    String pno;
    String dob;
    String nic;
    String doj;

    public Node(int d, String nam, String des, String sal, String dept, String age, String add, String pno, String dob, String nic, String doj) {
        data = d;
        this.nam = nam;
        this.des = des;
        this.sal = sal;
        this.dept = dept;
        this.age = age;
        this.add = add;
        this.pno = pno;
        this.dob = dob;
        this.nic = nic;
        this.doj = doj;
        prev = null;
        next = null;
    }
}

public class DLinkList {

    Node head;
    int size = 0;

    public DLinkList() {
        head = null;
    }

    public void insert(int d, String nam, String des, String sal, String dept, String age, String add, String pno, String dob, String nic, String doj) {
        size++;
        if (head == null) {
            head = new Node(d, nam, des, sal, dept, age, add, pno, dob, nic, doj);
            return;
        }

        if (head.data > d) {
            Node holder = head;
            Node newNode = new Node(d, nam, des, sal, dept, age, add, pno, dob, nic, doj);
            head = newNode;
            head.next = holder;
            holder.prev = newNode;
            return;
        }

        Node tmpNode = head;

        while (tmpNode.next != null && tmpNode.next.data < d) {
            tmpNode = tmpNode.next;
        }

        Node prevTmp = tmpNode;
        Node insertedNode = new Node(d, nam, des, sal, dept, age, add, pno, dob, nic, doj);

        if (tmpNode.next != null) {
            Node nextTmp = tmpNode.next;
            insertedNode.next = nextTmp;
            nextTmp.prev = insertedNode;
        }
        prevTmp.next = insertedNode;
        insertedNode.prev = prevTmp;
    }

    public void delete(int d) {
        size--;
        if (head == null) {
            System.out.println("The list is empty.");
            return;
        }

        if (head.data == d) {
            head = head.next;
            if (head != null) {
                head.prev = null;
            }
            return;
        }

        Node tmpNode = head;

        while (tmpNode != null && tmpNode.data != d) {
            tmpNode = tmpNode.next;
        }

        if (tmpNode == null) {
            System.out.println("That node does not exist in the list");
            return;
        }

        if (tmpNode.data == d) {
            tmpNode.prev.next = tmpNode.next;
            if (tmpNode.next != null) {
                tmpNode.next.prev = tmpNode.prev;
            }
        }
    }

    public void print() {
        Node tmpNode = head;

        while (tmpNode != null) {
            System.out.print(tmpNode.data + " -> ");
            tmpNode = tmpNode.next;
        }

        System.out.print("null");
    }

    public String[][] getAll() {
        Node tmp = head;

        String[][] data = new String[100][100];
        for (int i = 0; i < size; i++) {
            data[i][0] = Integer.toString(tmp.data);
            data[i][1] = tmp.nam;
            data[i][2] = tmp.des;
            data[i][3] = tmp.sal;
            data[i][4] = tmp.dept;
            data[i][5] = tmp.age;
            data[i][6] = tmp.add;
            data[i][7] = tmp.pno;
            data[i][8] = tmp.dob;
            data[i][9] = tmp.nic;
            data[i][10] = tmp.doj;
            tmp = tmp.next;
        }

        return data;
    }

    public String[][] getByid(int key) {
        Node tmp = head;
        while (tmp.data != key || tmp == null) {
            tmp = tmp.next;
        }
        String[][] data = new String[100][100];
        for (int i = 0; i < size; i++) {
            data[i][0] = Integer.toString(tmp.data);
            data[i][1] = tmp.nam;
            data[i][2] = tmp.des;
            data[i][3] = tmp.sal;
            data[i][4] = tmp.dept;
            data[i][5] = tmp.age;
            data[i][6] = tmp.add;
            data[i][7] = tmp.pno;
            data[i][8] = tmp.dob;
            data[i][9] = tmp.nic;
            data[i][10] = tmp.doj;

        }

        return data;
    }
}
